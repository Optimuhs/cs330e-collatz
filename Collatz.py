#!/usr/bin/env python3

# ---------------------------
# projects/collatz/Collatz.py
# Copyright (C) 2016
# Glenn P. Downing
# ---------------------------

# ------------
# collatz_read
# ------------


def collatz_read(s):
    """
    read two ints
    s a string
    return a list of two ints, representing the beginning and end of a range, [i, j]
    """
    a = s.split()
    return [int(a[0]), int(a[1])]

# -------------
# collatz
# Calculate cache for range i,j so that it isnt calculated again 
# -------------
def collatz_Cache(i,j):
  collatz_cache = {}
  for k in range(i, j + 1):
    n = k
    count = 1
    while n != 1:
      if (n % 2 == 0):
        n = n // 2
        if n in collatz_cache:
          count += collatz_cache[n]
          n = 1
          break
        else:
          count += 1
          
      else:
        n = (3 * n) + 1
        if n in collatz_cache:
          count += collatz_cache[n]
          n = 1
        else:
          count += 1
    collatz_cache[k] = count
  return collatz_cache
# ------------
# collatz_eval
# ------------


def collatz_eval(i, j):
  '''
  i the beginning of the range, inclusive
  j the end       of the range, inclusive
  return the max cycle length of the range [i, j]
  '''
  #Loop through the range 
	#Run collats conjecture
	#Store the key value pairs (number, cycle length) in dictionary
  #Find max for list of dict values 
  cache = collatz_Cache(1,10000)
  collatz_cache = cache

  #Swap numbers if j bigger than i
  if j < i:
    i, j = j, i 

  for k in range(i, j + 1):
    n = k
    count = 1
    while n != 1:
      if (n % 2 == 0):
        n = n // 2
        if n in collatz_cache:
          count += collatz_cache[n]
          n = 1
          break
        else:
          count += 1
          
      else:
        n = (3 * n) + 1
        if n in collatz_cache:
          count += collatz_cache[n]
          n = 1
        else:
          count += 1
    collatz_cache[k] = count
  
  #Make list of keys and values
  #Get indecies of i and j
  KeyList = list(collatz_cache.keys())
  ValList = list(collatz_cache.values())
  iIdx = KeyList.index(i)
  jIdx = KeyList.index(j)

  #If i and j are same number
  if i == j:
    ans = ValList[iIdx]
  else:
    ans = max(ValList[iIdx:jIdx + 1])
  return ans

# -------------
# collatz_print
# -------------


def collatz_print(w, i, j, v):
    """
    print three ints
    w a writer
    i the beginning of the range, inclusive
    j the end       of the range, inclusive
    v the max cycle length
    """
    w.write(str(i) + " " + str(j) + " " + str(v) + "\n")

# -------------
# collatz_solve
# -------------


def collatz_solve(r, w):
    """
    r a reader
    w a writer
    """
    for s in r:
        i, j = collatz_read(s)
        v = collatz_eval(i, j)
        collatz_print(w, i, j, v)

