import timeit

def cacheMake():
  code_to_test = """
i = 1
j = 999999
collatz_cache = {}
for k in range(i, j + 1):
  n = k
  count = 1
  while n != 1:
    if (n % 2 == 0):
      n = n // 2
      count += 1
    else:
      n = (3 * n) + 1
      count += 1
    collatz_cache[k] = count
    keyList = list(collatz_cache.values())
  ans = max(keyList)
  ansIdx = keyList.index(ans)
print(ans)
  """
  elapsed_time = timeit.timeit(code_to_test, number=100)/100
  print(str(elapsed_time))
cacheMake()

